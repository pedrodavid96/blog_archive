# Cleaning your `$HOME` with XDG

I've been configuring my [Arch Linux](https://www.archlinux.org/)(that's another blog post in itself)
and looking at my `$HOME`... It's definitely not clean.

So, does your `$HOME` also look like this?

```
pedro ~ $ ll
total <you don't want to know, this had to be cleaned>
drwxr-xr-x  76 pedro pedro       4096 Jun 21 18:59 ./
drwxr-xr-x   4 root  root        4096 Nov 24  2017 ../
-rw-rw-r--   1 pedro pedro        721 Mai 27 09:57 .bash_aliases
-rw-------   1 pedro pedro      47851 Jun 21 23:27 .bash_history
-rw-r--r--   1 pedro pedro        220 Nov 24  2017 .bash_logout
-rw-r--r--   1 pedro pedro       4584 Fev 13  2018 .bashrc
drwx------  37 pedro pedro       4096 Jun 18 16:43 .cache/
drwx------  50 pedro pedro       4096 Jun 21 16:53 .config/
drwxr-xr-x   2 pedro pedro       4096 Nov 27  2017 Desktop/
drwxr-xr-x  11 pedro pedro       4096 Mai 23 11:56 Documents/
drwxr-xr-x   9 pedro pedro       4096 Jun 18 12:18 Downloads/
-rw-rw-r--   1 pedro pedro      56562 Nov 29  2017 .git-completion.bash
-rw-rw-r--   1 pedro pedro        431 Jun  7 11:40 .gitconfig
-rw-rw-r--   1 pedro pedro      15922 Nov 29  2017 .git-prompt.sh
drwx------   3 pedro pedro       4096 Jun 18 11:58 .gnupg/
drwxrwxr-x   9 pedro pedro       4096 Mar  9  2018 .gradle/
drwxrwxr-x   3 pedro pedro       4096 Fev  8 02:55 .gradle-kotlin-dsl/
drwxrwxr-x   4 pedro pedro       4096 Nov 29  2017 .IdeaIC2017.2/
drwxrwxr-x   4 pedro pedro       4096 Dez  4  2017 .IdeaIC2017.3/
drwxrwxr-x   4 pedro pedro       4096 Abr  9  2018 .IdeaIC2018.1/
drwxrwxr-x   4 pedro pedro       4096 Dez  4  2018 .IdeaIC2018.3/
drwxrwxr-x   4 pedro pedro       4096 Mai 29 15:57 .IdeaIC2019.1/
drwxrwxr-x   3 pedro pedro       4096 Fev 28 17:03 .ivy2/
drwxrwxr-x   5 pedro pedro       4096 Abr 15 16:54 .java/
-rw-rw-r--   1 pedro pedro        456 Out 29  2018 .kotlinc_history
-rw-------   1 pedro pedro       1792 Jun 19 16:19 .lesshst
drwx------   5 pedro pedro       4096 Jun  3 17:45 .local/
drwx------   5 pedro pedro       4096 Nov 27  2017 .mozilla/
-rw-------   1 pedro pedro         26 Jul 19  2018 .node_repl_history
drwxrwxr-x 375 pedro pedro      12288 Mai 31 17:01 .npm/
drwx------   5 pedro pedro       4096 Jun 11 10:15 .password-store/
-rw-r--r--   1 pedro pedro        884 Mar 14  2018 .profile
-rw-------   1 pedro pedro       3509 Abr 17 15:27 .python_history
drwx------   3 pedro pedro       4096 Mai 30 18:14 .ssh/
drwxr-xr-x   4 pedro pedro       4096 Jun  3 17:45 .vim/
-rw-------   1 pedro pedro      29598 Jun 21 18:59 .viminfo
-rw-rw-r--   1 pedro pedro        515 Abr 13  2018 .vimrc
-rw-------   1 pedro pedro         55 Jun 18 11:58 .Xauthority
-rw-rw-r--   1 pedro pedro        131 Set 25  2018 .xinputrc
```

Clearly something had to be done. And I shouldn't be the only one with this problem.
Before creating [yet another standard](https://xkcd.com/927/) I stumbled upon the
[*XDG Base Directory Specification*](https://specifications.freedesktop.org/basedir-spec/basedir-spec-0.6.html)
which, in short, specifies where the files should be placed in the system.

## XDG summary

There are 3 different types of folders used for storage.

* DATA: user specific data files
* CONFIG: user specific configuration files
* CACHE: user specific non-essential data files

These should be referenced by the `$XDG_DATA_HOME`, `$XDG_CONFIG_HOME` and `$XDG_CACHE_HOME` environment variables,
respectively.

## What now?

The standard is already set.
Hopefully modern software already follows this standard.

Given the target audience of this blog, you're probably a developer.
You can make the difference by simply adhering to this.

But not every piece of software is following it.
It's safe to say that the probability of it adhering to this standard is
inversely proportional of it's age.

Some of them have *"partial support"* which might require some workaround
on a way or another.

As usual, [Arch Linux documentation](https://wiki.archlinux.org/index.php/XDG_Base_Directory) is on point,
deserving the :goldstar: of this post.

## What I did?

Some of them were as simple as adding an environment variable specific to the software in question,
some where a little bit more tricky.
I will cover from the basics to the ones which gave more trouble (in that order).

1. Add *XDG* environment variables to my `.bashrc`.
```.bashrc
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CACHE_HOME=$HOME/.cache
```

2. Xorg

Move files to config folder

```.bashrc
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc

export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
```

These variables are respected by `xinit` but not `startx`, so I specified them when calling it.

```.bashrc
exec startx "$XDG_CONFIG_HOME/X11/xinitrc"
```

3. `npm`
`npm` uses runtime variables as storage folders.
We can supply a configuration file where those variables can be set and
that file path can be set through an environment variable.


```.bashrc
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
```

```npmrc
prefix=${XDG_DATA_HOME}/npm
cache=${XDG_CACHE_HOME}/npm
tmp=${XDG_RUNTIME_DIR}/npm
init-module=${XDG_CONFIG_HOME}/npm/config/npm-init.js
```

4. `python`
`python` interactive prompt uses a `~/.python_history` file.
Similarly to `npm` we can supply a script (in this case, in python) to be executed
before the first prompt is displayed in interactive mode.

```.bashrc
export PYTHONSTARTUP=$XDG_CONFIG_HOME/python/pythonrc.py
```

With this we can change the default behavior and write this file somewhere else.

```pythonrc.py
import atexit
import os
import readline

histfile = os.path.join(os.path.normpath(os.environ["XDG_CACHE_HOME"]), "python", ".python_history")
try:
    readline.read_history_file(histfile)
    # default history len is -1 (infinite), which may grow unruly
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)
```

5. vim
Not necessarily the hardest, but it has some magic and I had some troubles with my
plugin manager ([vim-plug](https://github.com/junegunn/vim-plug)).

We can supply an [Ex](http://vimdoc.sourceforge.net/htmldoc/intro.html#Ex) command through `$VIMINIT`.
That same command can be used to load our "missplaced" configuration.

```.bashrc
export VIMINIT=":source $XDG_CONFIG_HOME"/vim/.vimrc
```

After that is a matter of configuring the right variables, not unlike `npm`.

```vimrc
"--------------------------- Use XDG folders ----------------------------------"
set undodir=$XDG_CACHE_HOME/vim/undo
set directory=$XDG_CACHE_HOME/vim/swap
set backupdir=$XDG_CACHE_HOME/vim/backup
set viminfo+='1000,n$XDG_CACHE_HOME/vim/viminfo
set runtimepath=$XDG_CONFIG_HOME/vim,$VIMRUNTIME,$XDG_CONFIG_HOME/vim/after
"------------------------------------------------------------------------------"
```

I also removed the folder from `call plug#begin(<folder>)` and it works 🤷.

**Note:** I had to set XDG folders before calling `plug#begin()`.

## Final Result

```
pedro ~ $ ll
total 20
drwx------ 1 pedro pedro  242 Jun 21 23:23 ./
drwxr-xr-x 1 root  root    10 Jan 29 21:35 ../
-rw------- 1 pedro pedro 7600 Jun 21 23:23 .bash_history
-rw-r--r-- 1 pedro pedro   21 Jan  7 16:11 .bash_logout
-rw-r--r-- 1 pedro pedro   58 Feb  2 02:28 .bash_profile
lrwxrwxrwx 1 pedro pedro   38 Jun 21 02:52 .bashrc -> /home/pedro/Documents/dotfiles/.bashrc
drwx------ 1 pedro pedro  456 Jun 21 22:42 .cache/
drwxr-xr-x 1 pedro pedro  284 Jun 21 23:05 .config/
drwxr-xr-x 1 pedro pedro  806 Jun 21 23:39 Documents/
drwxr-xr-x 1 pedro pedro    0 Jun 21 01:49 Downloads/
drwx------ 1 pedro pedro  150 Jun  7 00:15 .gnupg/
drwxr-xr-x 1 pedro pedro   24 Apr  5 01:02 .IdeaIC2019.1/
drwxr-xr-x 1 pedro pedro   30 Jan 30 01:18 .java/
drwxr-xr-x 1 pedro pedro   10 Jan 29 23:57 .local/
drwx------ 1 pedro pedro   72 Jan 30 00:46 .mozilla/
drwxr-xr-x 1 pedro pedro   44 Feb  1 00:22 opt/
drwx------ 1 pedro pedro  178 May 30 21:37 .ssh/
```

(Want to know more about that [`.bashrc`](gitlab/bashrc/raw) [symlink](./symlinks.md)?)
